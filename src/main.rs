use cgmath::prelude::*;
use cgmath::{Rad, Point3, Quaternion};

use spade::SpatialObject;
use spade::rtree::RTree;
use spade::BoundingRect;

use rand::prelude::*;

mod constants;
use constants::*;

mod orbit;
use orbit::{Orbit};

struct CelestialBody {
    radius: f64,
    sub_bodies: Vec<(Orbit, CelestialBody)>
}

impl CelestialBody {
    fn new(radius: f64) -> CelestialBody {
        CelestialBody {
            radius,
            sub_bodies: Vec::new(),
        }
    }

    fn get_bounding_size(&self) -> f64 { // This is a very rudamentary bounding sphere to be turned into a box
        let mut bounding_size = self.radius;

        for (orbit, sub_body) in self.sub_bodies.iter() {
            let sub_bounding_size = orbit.get_bounding_size() + sub_body.get_bounding_size();

            if sub_bounding_size > bounding_size {
                bounding_size = sub_bounding_size;
            }
        }

        bounding_size
    }
}

struct StarSystem {
    body: CelestialBody,
    position: Point3<f64>,
}

impl SpatialObject for StarSystem {
    type Point = Point3<f64>;

    fn mbr(&self) -> BoundingRect<Point3<f64>> {
        let bounding_size = self.body.get_bounding_size();

        BoundingRect::from_corners(
            &self.position.add_element_wise(bounding_size),
            &self.position.sub_element_wise(bounding_size)
        )
    }
    
    fn distance2(&self, point: &Point3<f64>) -> f64 {
        (self.position - point).magnitude2()
    }
}

struct Galaxy {
    systems: RTree<StarSystem>
}

impl Galaxy {
    fn new() -> Galaxy {
        Galaxy { systems: RTree::new() }
    }

    fn new_with_stars(n: u32) -> Galaxy {
        let mut galaxy = Galaxy::new();

        let mut rng = thread_rng();

        let star_y_distrobution = rand::distributions::Normal::new(0.0, 1.0 * KILO * LIGHT_YEAR);
        let star_r_distrobution = rand::distributions::Normal::new(0.0, 100.0 * KILO * LIGHT_YEAR);

        for _ in 0..n {
            let star_y = star_y_distrobution.sample(&mut rng);
            let star_r = star_r_distrobution.sample(&mut rng);
            let star_angle = random::<f64>() * 2.0 * PI;

            let star_position = Quaternion::<f64>::from_angle_y(Rad(star_angle)).rotate_point(Point3::new(star_r, star_y, 0.0));

            let star_system = StarSystem {
                position: star_position,
                body: CelestialBody::new(695.51 * MEGA)
            };

            galaxy.systems.insert(star_system);
        }

        galaxy
    }
}

fn main() {


    println!("Hello, world!");
}
